<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Schedule: SFD2010</title>
<style type="text/css">
th {
	border: thin solid #CCC !important;
	background-color:#6F9 !important;
}

td {
	border: thin solid #CCC !important;
}
</style>
</head>

<body>
<h1>Schedule SFD 2010 (18th of September 2010)</h1>

<table border="1" cellspacing="0" cellpadding="5" summary="Schedule for SFD 2010">

  <tr>
    <th width="87" scope="col">Timings</th>
    <th width="281" scope="col">Event and Details</th>
    <th width="281" scope="col">Venue</th>
  </tr>
  <tr>
    <td>9:00</td>
    <td>Event starts</td>
    <td rowspan="5">Sanskriti Kaksh</td>
  </tr>
  <tr>
    <td>9:15-9:45</td>
    <td>Quiz contest </td>
  </tr>
  <tr>
    <td>9:50-11:15</td>
    <td>Presentation contest</td>
  </tr>
  <tr>
    <td>11:20-11:50</td>
    <td>Open your mind</td>
  </tr>
  <tr>
    <td>12:00:1:00</td>
    <td>Debate</td>
  </tr>
  <tr>
    <td>1:00-1:30</td>
    <td>Hurry up ! Time is out!!</td>
    <td>Internet Lab</td>
  </tr>
  <tr>
    <td>Before 1:30</td>
    <td>Submissions (Photography contest)</td>
    <td>Sanskriti Kaksh</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td rowspan="9">Sanskriti Kaksh</td>
  </tr>
  <tr>
    <td>2:30-2:45</td>
    <td>Formal function starts</td>
  </tr>
  <tr>
    <td>2:45-3:15</td>
    <td>Welcome note </td>
  </tr>
  <tr>
    <td>3:15-3:30 </td>
    <td>Lecture on SFD importance and open source technology</td>
  </tr>
  <tr>
    <td>3:30-4:00</td>
    <td>Introductory Presentation</td>
  </tr>
  <tr>
    <td>4:00-5:15</td>
    <td><strong>Guest lecture by Mr. Narendra Sisodiya</strong></td>
  </tr>
    <tr>
    <td>5:15-5:30</td>
    <td>Prize Distributions</td>
  </tr>
    <tr>
    <td>5:30-5:45</td>
    <td>Vote of thanks</td>
  </tr>
  
      <tr>
    <td>finally in the end</td>
    <td>Refreshments</td>
  </tr>


</table>

<h2>See also:</h2>

<ul>
  <li><a href="event_details.html">Event rules and regulations.</a></li>

<li><a href="./register/">Register for the events.</a></li></ul>

</body>
</html>