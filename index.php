<?php
require ("_php/helpers.php");
?>
<?php 
	/* Local configurations
	***********************************************/	
	$site_title = 'OSUM SMVDU CLUB';
	$site_type = 'Blog';
	$site_description = "Official website of OSUM SMVDU Club. A programmer's group of Shri Mata Vaishno Devi University.";
	$site_keywords = "osum, smvdu, smvd university, shri mata vaishno devi university, competition, opensource, programming, blog";
	$site_photo_url = "";
	$site_author = "Saurabh Kumar and contributors";
	
	$site_twitter_url = "https://twitter.com/osum_club";
	$site_forum_url = "http://groups.google.com/group/osumsmvduclub";
?>

<!doctype html>
<html itemscope itemtype="http://schema.org/<?php print($site_type); ?>" class=no-js>
<head>
<head>
	<meta charset="utf-8">
	
	<title>	<?php print($site_title); ?> </title>
		
	<meta name="viewport" content="width=device-width,initial-scale=1">	

	<meta itemprop="name" content="<?php print($site_title)?>">
	<meta name="description" content="<?php print($site_description);?>">
	<meta itemprop="description" content="<?php print($site_description);?>">
	<meta itemprop="image" content="<?php print($site_photo_url);?>">
	<meta name="keywords" content="<?php print($site_keywords);?>">
	<meta name="author" content="<?php print($site_author);?>">

	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">


	<link rel="stylesheet" type="text/css" href="css/layout.css">
	<link rel="stylesheet" type="text/css" href="css/spritely.css">
	<link rel="stylesheet" type="text/css" href="css/montage.css">
	
	<link rel="stylesheet" href="css/style.css" />
	
	
	<script src="/js/libs/modernizr-2.0.6.min.js"></script>	
	
</head>
<body id="" class="osumsite">
<div id="container">
	<header>
		<div id="header">
		  <div id="sky"></div>
		  <div id="city1"></div>
		  <div id="city2"></div>
		  <div id="layer1"></div>
		  <div id="plane1"></div>
		</div>
	</header>

    <div id="main" role="main">
    	
		<div class="body">
    <div class="page_wrap">
		<div class="content" id="content_load">
      <h1>OSUM SMVDU Club</h1>
		<p id="about"> 
			OSUM SMVDU club was officially started at <a href="http://smvdu.net.in">SMVDU</a> in 2007. This club aims at promoting open source software. In order to promote this idea we have been regularly carrying out discussions and various activities regarding the same in the campus, inviting guests and students from outside. 
		</p>
      

		<h1>What is Software Freedom Day??</h1>
		<p>
			<a href="http://softwarefreedomday.org/">Software Freedom Day</a> (SFD) is 
			a worldwide celebration of Free and Open Source Software (<a href=http://en.wikipedia.org/wiki/Free_and_open_source_software >FOSS</a>). 
			The non-profit company Software Freedom International coordinates SFD at a global
			level, providing support, giveaways and a point of collaboration, but volunteer 
			teams around the world organize the local SFD events to impact their own communities.
       </p>
      
		<h1>Goal</h1>
		<p>
		To educate people world-wide about the benefits of using high quality FOSS in education, in government, at home, and in business -- in short, everywhere!</p>

     <div class="latest">
     <h1>SFD 2011 Celebration</h1>
     <p>
		 We celebrated Software Freedom Day on 17<sup>th</sup> September 2011, where the chief guest was 
		 <a href="http://smvdu.net.in/faculty-cse/209-garg-m-l.html" >Prof. M.L.Garg</a> and 
		 we had guest lecture by Manmeet Singh, Assistant Professor, BGSB University who enlightened 
		 the audience with the benefits of open source and its "Open Source Licensing".
		 <a href="sfd2011/images/sfd2011_main_thumb.jpg" title="View enlarged" alt="PHOTO" />
		 <img src="sfd2011/images/sfd2011_main_thumb.jpg" style="float: right" /></a>
     </p>
     
     <p>
		 Prof. R. N. K. Bamezai, Honorable <a href="http://smvdu.net.in/vcmsgmenu.html">Vice Chancellor</a>, 
		 <a href="http://smvdu.net.in" >Shri Mata Vaishno Devi University</a> who is very keen and supportor
		 of open, like previous year, gave a surprise appearance and addressed the participants and organisors.
     </p>
     
     
     <p>
		 Enthusiatic audience mainly from the various <a href="http://smvdu.net.in/academics/college-of-engineering.html" >
		 disciplines</a> of engineering not only show-case their skills with <a href="http://en.wikipedia.org/wiki/List_of_free_and_open_source_software_packages">open-source tools</a> 
		 and technologies, but also showed others how they can be the future for a "open world".
     </p>
     
     <p>
		 The OSUM SMVDU Club would like to congratulate all winners, participants and organising members for their 
		 contribution in making "Software Freedom Day 2011" a success and wishes them best of luck for the future work.
     
     </p>
		 This note will not be complete if we don't mention the name of our main sponsors for the event - 
		 <a href="http://www.agmatel.com/">Agmatel India Private Limited</a> and 
		 <a href="http://yellowpages.sulekha.com/delhi/business-services/software-companies/east-of-kailash/amtrak-technologies-pvt-ltd.htm#">
		 Amtrak Technologies</a>, and of course <a href="http://smvdu.net.in">Shri Mata Vaishno Devi University</a> for its invaluable support.
     <p>
     
     <p>
		 Saurabh Kumar/ <a href="http://saurabhworld.in/go/twitter">@saurabh_world</a><br />
		 OSUM SMVDU Club
     </p>
     
   </div><!--end latest div-->     
     
     <h1>Photo Gallery: SFD 2010</h1>
	 <img src=images/ajax-loader.gif class=loading></img>
     <div class="am-container" id="g-sfd2010" style="width:100%;margin:0px auto;">

     <?php include("list.photos.php"); 
	 ?>
     
     </div>
     
     <h1>Photo Gallery: SFD 2009 </h1>
	 <img src=images/ajax-loader.gif class=loading></img>
     <p class="gallery" id="g-sfd2009"><img src="sfd2009/images/sfd2009_smvdu (1).JPG" />
     
     <img src="sfd2009/images/sfd2009_smvdu (10).JPG"/>
     <img src="sfd2009/images/sfd2009_smvdu (11).JPG" /><img src="sfd2009/images/sfd2009_smvdu (12).JPG"/><img src="sfd2009/images/sfd2009_smvdu (13).JPG"/><img src="sfd2009/images/sfd2009_smvdu (14).JPG"/><img src="sfd2009/images/sfd2009_smvdu (2).JPG" /><img src="sfd2009/images/sfd2009_smvdu (3).JPG"/><img src="sfd2009/images/sfd2009_smvdu (4).JPG"/><img src="sfd2009/images/sfd2009_smvdu (6).JPG"/><img src="sfd2009/images/sfd2009_smvdu (7).JPG"/><img src="sfd2009/images/sfd2009_smvdu (8).JPG"/><img src="sfd2009/images/sfd2009_smvdu (9).JPG"/><img src="sfd2009/images/sfd2009_smvdu.JPG"/>
     </p>
	
    <h1>Join the Club</h1>
     <p>
     To join the club and participate in the club activities, follow the links below.
     <table style="background-color: #fff; padding: 5px;" cellspacing=0>
  <tr>
    <td>
		<a href="http://groups.google.com/group/osumsmvduclub" title="Join the group" target="_blank"><img src="http://groups.google.com/intl/en/images/logos/groups_logo_sm.gif"
         height=30 width=140 alt="Google Groups"></a>
    </td></tr>
  <tr>
    <td style="padding-left: 5px">
  <a href="<?php print($site_forum_url);?>" target="_blank">Visit this group</a>
  </td></tr>
</table>
     
     </p>
	</div>
	

    </div>
    
    
   
    <div class="sidebar">
    	<div class="rtcontent">         
      <h2> Important links </h2>
       <ul>
           <li><a class=external href="<?php print($site_twitter_url);?>">Follow us on Twitter</a></li>
           <li><a class=external href="<?php print($site_forum_url);?>">Join Discussion Group</a></li>           
       </ul>
      <p>
      <form action="http://groups.google.com/group/osumsmvduclub/boxsubscribe">
        <tr>
          <td style="padding-left: 5px;">
            <input name="email" type="text" class="inputtext" value="Enter your E-mail here" />
          <input type=submit name="sub" value="Subscribe"></td>
        </tr>
      </form>
      </p>
      <hr />
      <p>

      </p>

          <img src="images/osumlgod.jpg" width="205" height="138" />
          
          
      </div>
    </div>
    
  </div>
</div>

	</div>
	
	<footer class=clearfix>

    </footer>
	

  <?php /* Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline 
  **********************************************************************************/?>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.6.2.min.js"><\/script>')</script>

  <!-- scripts concatenated and minified via ant build script-->
  <script defer src="js/plugins.js"></script>
  <script defer src="js/script.js"></script>
  <!-- end scripts-->

	
  <?php /* Change UA-XXXXX-X to be your site's ID 
  *******************************************************************************************/ ?>
  <script>
    window._gaq = [['_setAccount','UA-16351077-6'],['_trackPageview'],['_trackPageLoadTime']];
    Modernizr.load({
      load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
    });
  </script>


  <?php /* Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
  chromium.org/developers/how-tos/chrome-frame-getting-started 
  *******************************************************************************************/ ?>
  
  <!--[if lt IE 7 ]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->
  
  
</body>
</html>
