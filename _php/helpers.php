<?php
function get_domain()
{
	//the variable $domain can be set to the domain your application is running from.
//	$domain = "http://www.smvdu.net.in/";
	$domain = $_SERVER['HTTP_HOST'];
   	return $domain;
}



function mailto($email, $text, $title, $extras) //1
{
	$link = '<a href=\"mailto:' . $email; //2
	$link = str_rot13($link); //3

	$data = '<script type="text/javascript">document.write("'; //4
	$data .= $link; //5
	$data .= '".replace(/[a-zA-Z]/g, function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);}));
</script>';
	$data .= '"';

	if ($title) //6
	{
		$data .= ' title="' . $title . '"';
	}
	else
	{
		$data .= ' title="' . $text . '"';
	}

	if (is_array($extras))
	{
		foreach($extras as $rule)
		{
			$data .= parse_extras($rule);
		}
	}

	if (is_string($extras))
	{
		$data .= parse_extras($extras);
	}
	$data .= ">";

	$data .= $text;
	$data .= "</a>"; //7
	return $data; //8
}

function parse_extras($rule)
{
	if ($rule[0] == "#") //1
	{
		$id = substr($rule,1,strlen($rule)); //2
		$data = ' id="' . $id . '"'; //3
		return $data;
	}

	if ($rule[0] == ".") //4
	{
		$class = substr($rule,1,strlen($rule));
		$data = ' class="' . $class . '"';
		return $data;
	}

	if ($rule[0] == "_") //5
	{
		$data = ' target="' . $rule . '"';
		return $data;
	}
}


function anchor($link, $text, $title, $extras)//1
{
	$domain = get_domain();
	$link = $domain . $link;
	$data = '<a href="' . $link . '"';

	if ($title)
	{
		$data .= ' title="' . $title . '"';
	}
	else
	{
		$data .= ' title="' . $text . '"';
	}

	if (is_array($extras))//2
	{
		foreach($extras as $rule)//3
		{
			$data .= parse_extras($rule);//4
		}
	}

	if (is_string($extras))//5
	{
		$data .= parse_extras($extras);//6
	}

	$data.= '>';

	$data .= $text;
	$data .= "</a>";

	return $data;
}


?>