<?php
require_once( dirname(__FILE__).'/form.lib.php' );

define( 'PHPFMG_USER', "thes.kumar@gmail.com" ); // must be a email address. for sending password to you.
define( 'PHPFMG_PW', "osum123" );

?>
<?php
/**
 * Copyright (CC) : Saurabh Kumar
*/

# main
# ------------------------------------------------------
error_reporting( E_ERROR ) ;
phpfmg_admin_main();
# ------------------------------------------------------




function phpfmg_admin_main(){
    $mod  = isset($_REQUEST['mod'])  ? $_REQUEST['mod']  : '';
    $func = isset($_REQUEST['func']) ? $_REQUEST['func'] : '';
    $function = "phpfmg_{$mod}_{$func}";
    if( !function_exists($function) ){
        phpfmg_admin_default();
        exit;
    };

    // no login required modules
    $public_modules   = false !== strpos('|captcha|', "|{$mod}|");
    $public_functions = false !== strpos('|phpfmg_mail_password||phpfmg_filman_download||phpfmg_image_processing||phpfmg_dd_lookup|', "|{$function}|") ;   
    if( $public_modules || $public_functions ) { 
        $function();
        exit;
    };
    
    return phpfmg_user_isLogin() ? $function() : phpfmg_admin_default();
}

function phpfmg_admin_default(){
    if( phpfmg_user_login() ){
        phpfmg_admin_panel();
    };
}



function phpfmg_admin_panel()
{    
    phpfmg_admin_header();
    phpfmg_writable_check();
?>    
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td valign=top style="padding-left:280px;">

<style type="text/css">
    .fmg_title{
        font-size: 16px;
        font-weight: bold;
        padding: 10px;
    }
    
    .fmg_sep{
        width:32px;
    }
    
    .fmg_text{
        line-height: 150%;
        vertical-align: top;
        padding-left:28px;
    }

</style>


<div class="fmg_title">
    1. Email Traffics
</div>
<div class="fmg_text">
    <a href="admin.php?mod=log&func=view&file=1">view</a> &nbsp;&nbsp;
    <a href="admin.php?mod=log&func=download&file=1">download</a>
</div>


<div class="fmg_title">
    2. Form Data
</div>
<div class="fmg_text">
    <a href="admin.php?mod=log&func=view&file=2">view</a> &nbsp;&nbsp;
    <a href="admin.php?mod=log&func=download&file=2">download</a>
</div>


<div class="fmg_title">
    3. Form Generator
</div>
<div class="fmg_text">
    <a href="#" onClick="document.frmFormMail.submit(); return false;" title="<?php echo htmlspecialchars(PHPFMG_SUBJECT);?>">Edit Form</a> &nbsp;&nbsp;
    <!--<a href="http://www.formmail-maker.com/generator.php" >New Form</a> -->
</div>
    <form name="frmFormMail" action='http://www.formmail-maker.com/generator.php' method='post' enctype='multipart/form-data'>
    <input type="hidden" name="uuid" value="<?php echo PHPFMG_ID; ?>">
    <input type="hidden" name="external_ini" value="<?php echo phpfmg_formini(); ?>">
    </form>

		</td>
	</tr>
</table>

<?php
    phpfmg_admin_footer();
}



function phpfmg_admin_header( $title = '' ){
    header( "Content-Type: text/html; charset=" . PHPFMG_CHARSET );
?>
<html>
<head>
    <title><?php echo '' == $title ? '' : $title . ' | ' ; ?>SFD2010: OSUM SMVDU Club Admin Panel </title>
    <meta name="author" content="Saurabh Kumar, saurabh.kumar[at]smvdu.net.in">
    <meta name="generator" content="">

    <style type='text/css'>
    body, td, label, div, span{
        font-family : Verdana, Arial, Helvetica, sans-serif;
        font-size : 12px;
    }
    </style>
</head>
<body  marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">

<table cellspacing=0 cellpadding=0 border=0 width="100%">
    <td nowrap align=center style="background-color:#024e7b;padding:10px;font-size:18px;color:#ffffff;font-weight:bold;width:250px;" >
        Form Admin Panel
    </td>
    <td style="padding-left:30px;background-color:#86BC1B;width:100%;font-weight:bold;" >
        &nbsp;
<?php
    if( phpfmg_user_isLogin() ){
        echo '<a href="admin.php" style="color:#ffffff;">Main Menu</a> &nbsp;&nbsp;' ;
        echo '<a href="admin.php?mod=user&func=logout" style="color:#ffffff;">Logout</a>' ;
    }; 
?>
    </td>
</table>

<div style="padding-top:28px;">

<?php
    
}


function phpfmg_admin_footer(){
?>

</div>

<div style="color:#cccccc;text-decoration:none;padding:18px;font-weight:bold;">
	:: <a href="http://smvdu.net.in" target="_blank" title="Visit: Main Page of SMVDU" style="color:#cccccc;font-weight:bold;text-decoration:none;">Shri Mata Vaishno Devi University</a> ::
</div>

</body>
</html>
<?php
}


function phpfmg_image_processing(){
    $img = new phpfmgImage();
    $img->out_processing_gif();
}


# phpfmg module : captcha
# ------------------------------------------------------
function phpfmg_captcha_get(){
    $img = new phpfmgImage();
    $img->out();
    $_SESSION[PHPFMG_ID.'fmgCaptchCode'] = $img->text ;
}



function phpfmg_captcha_generate_images(){
    for( $i = 0; $i < 50; $i ++ ){
        $file = "$i.png";
        $img = new phpfmgImage();
        $img->out($file);
        $data = base64_encode( file_get_contents($file) );
        echo "'{$img->text}' => '{$data}',\n" ;
        unlink( $file );
    };
}


function phpfmg_dd_lookup(){
    $paraOk = ( isset($_REQUEST['n']) && isset($_REQUEST['lookup']) && isset($_REQUEST['field_name']) );
    if( !$paraOk )
        return;
        
    $base64 = phpfmg_dependent_dropdown_data();
    $data = @unserialize( base64_decode($base64) );
    if( !is_array($data) ){
        return ;
    };
    
    
    foreach( $data as $field ){
        if( $field['name'] == $_REQUEST['field_name'] ){
            $nColumn = intval($_REQUEST['n']);
            $lookup  = $_REQUEST['lookup']; // $lookup is an array
            $dd      = new DependantDropdown(); 
            echo $dd->lookupFieldColumn( $field, $nColumn, $lookup );
            return;
        };
    };
    
    return;
}


function phpfmg_filman_download(){
    if( !isset($_REQUEST['filelink']) )
        return ;
        
    $info =  @unserialize(base64_decode($_REQUEST['filelink']));
    if( !isset($info['recordID']) ){
        return ;
    };
    
    $file = PHPFMG_SAVE_ATTACHMENTS_DIR . $info['recordID'] . '-' . $info['filename'];
    phpfmg_util_download( $file, $info['filename'] );
}


class phpfmgDataManager
{
    var $dataFile = '';
    var $columns = '';
    var $records = '';
    
    function phpfmgDataManager(){
        $this->dataFile = PHPFMG_SAVE_FILE; 
    }
    
    function parseFile(){
        $fp = @fopen($this->dataFile, 'rb');
        if( !$fp ) return false;
        
        $i = 0 ;
        $phpExitLine = 1; // first line is php code
        $colsLine = 2 ; // second line is column headers
        $this->columns = array();
        $this->records = array();
        $sep = chr(0x09);
        while( !feof($fp) ) { 
            $line = fgets($fp);
            $line = trim($line);
            if( empty($line) ) continue;
            $line = $this->line2display($line);
            $i ++ ;
            switch( $i ){
                case $phpExitLine:
                    continue;
                    break;
                case $colsLine :
                    $this->columns = explode($sep,$line);
                    break;
                default:
                    $this->records[] = explode( $sep, phpfmg_data2record( $line, false ) );
            };
        }; 
        fclose ($fp);
    }
    
    function displayRecords(){
        $this->parseFile();
        echo "<table border=1 style='width=95%;border-collapse: collapse;border-color:#cccccc;' >";
        echo "<tr><td>&nbsp;</td><td><b>" . join( "</b></td><td>&nbsp;<b>", $this->columns ) . "</b></td></tr>\n";
        $i = 1;
        foreach( $this->records as $r ){
            echo "<tr><td align=right>{$i}&nbsp;</td><td>" . join( "</td><td>&nbsp;", $r ) . "</td></tr>\n";
            $i++;
        };
        echo "</table>\n";
    }
    
    function line2display( $line ){
        $line = str_replace( array('"' . chr(0x09) . '"', '""'),  array(chr(0x09),'"'),  $line );
        $line = substr( $line, 1, -1 ); // chop first " and last "
        return $line;
    }
    
}
# end of class



# ------------------------------------------------------
class phpfmgImage
{
    var $im = null;
    var $width = 73 ;
    var $height = 33 ;
    var $text = '' ; 
    var $line_distance = 8;
    var $text_len = 4 ;

    function phpfmgImage( $text = '', $len = 4 ){
        $this->text_len = $len ;
        $this->text = '' == $text ? $this->uniqid( $this->text_len ) : $text ;
        $this->text = strtoupper( substr( $this->text, 0, $this->text_len ) );
    }
    
    function create(){
        $this->im = imagecreate( $this->width, $this->height );
        $bgcolor   = imagecolorallocate($this->im, 255, 255, 255);
        $textcolor = imagecolorallocate($this->im, 0, 0, 0);
        $this->drawLines();
        imagestring($this->im, 5, 20, 9, $this->text, $textcolor);
    }
    
    function drawLines(){
        $linecolor = imagecolorallocate($this->im, 210, 210, 210);
    
        //vertical lines
        for($x = 0; $x < $this->width; $x += $this->line_distance) {
          imageline($this->im, $x, 0, $x, $this->height, $linecolor);
        };
    
        //horizontal lines
        for($y = 0; $y < $this->height; $y += $this->line_distance) {
          imageline($this->im, 0, $y, $this->width, $y, $linecolor);
        };
    }
    
    function out( $filename = '' ){
        if( function_exists('imageline') ){
            $this->create();
            if( '' == $filename ) header("Content-type: image/png");
            ( '' == $filename ) ? imagepng( $this->im ) : imagepng( $this->im, $filename );
            imagedestroy( $this->im ); 
        }else{
            $this->out_predefined_image(); 
        };
    }

    function uniqid( $len = 0 ){
        $md5 = md5( uniqid(rand()) );
        return $len > 0 ? substr($md5,0,$len) : $md5 ;
    }
    
    function out_predefined_image(){
        header("Content-type: image/png");
        $data = $this->getImage(); 
        echo base64_decode($data);
    }
    
    // predefined random images
    function getImage(){
        $images = array(
			'F987' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaUlEQVR4nGNYhQEaGAYTpIn7QkMZQxhCGUNDkMQCGlhbGR0dGkRQxEQaXUEkmpgjUF0AkvtCo5YuzQpdtTILyX0BDYyBQHWtDCh6GUDmTUEVYwGJBTBguMXRAVUM7GYUsYEKPypCLO4DAKIrzSg+ue3qAAAAAElFTkSuQmCC',
			'66CC' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZUlEQVR4nGNYhQEaGAYTpIn7WAMYQxhCHaYGIImJTGFtZXQICBBBEgtoEWlkbRB0YEEWaxBpYG1gdEB2X2TUtLClq1ZmIbsvZIpoK5I6iN5WkUZXrGKodmBzCzY3D1T4URFicR8A4Q/LNaxwSpgAAAAASUVORK5CYII=',
			'A014' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpIn7GB0YAhimMDQEIImxBjCGMIQwNCKLiUxhbQWKtiKLBbSKNDpMYZgSgOS+qKXTVmZNWxUVheQ+iDpGB2S9oaFgsdAQFPNYW9HdEgCyFUOMIYAx1AFFbKDCj4oQi/sAaaPNkRuOmLUAAAAASUVORK5CYII=',
			'D3C0' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAW0lEQVR4nGNYhQEaGAYTpIn7QgNYQxhCHVqRxQKmiLQyOgRMdUAWa2VodG0QCAhAFWtlbWB0EEFyX9TSVWFLV63MmobkPjR1SOZhE0OzA4tbsLl5oMKPihCL+wABeM2mp0S3QwAAAABJRU5ErkJggg==',
			'A134' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbUlEQVR4nGNYhQEaGAYTpIn7GB0YAhhDGRoCkMRYAxgDWBsdGpHFRKawBgBVtSKLBbQyBDA0OkwJQHJf1NJVUaumroqKQnIfRJ2jA7Le0FCgWENgaAi6eUCXoNsBdAuaGGsoupsHKvyoCLG4DwDbb80LYQ4GEQAAAABJRU5ErkJggg==',
			'36C1' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAX0lEQVR4nGNYhQEaGAYTpIn7RAMYQxhCHVqRxQKmsLYyOgRMRVHZKtLI2iAQiiI2RaSBFSiD7L6VUdPClq5atRTFfVNEW5HUwc1zxSomgM0tKGJQN4cGDILwoyLE4j4AZC3LqJvfHMQAAAAASUVORK5CYII=',
			'4B27' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcUlEQVR4nM2QMQ6AMAgAYfAH+J86uGNSfISvqEN/gP1D+0rrRtVRo9x2geQClMsE+BPv9GnvQVC8dZ4iDi6QcehpHQM3rlOKUB2bvpS2ueQlL6aPj70DcytCq1PQtqU6Bj65iA7dubmTqXVf/e85bvp2a37LZCiOcHEAAAAASUVORK5CYII=',
			'ED1E' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAVklEQVR4nGNYhQEaGAYTpIn7QkNEQximMIYGIIkFNIi0MoQwOjCgijU6YhFzmAIXAzspNGrayqxpK0OzkNyHpo5UsVYGNDGQmxlDHVHcPFDhR0WIxX0AWfDLmnpR/MIAAAAASUVORK5CYII=',
			'31B4' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaUlEQVR4nGNYhQEaGAYTpIn7RAMYAlhDGRoCkMQCpjAGsDY6NCKLMbSyBrA2BLSiiE1hAKmbEoDkvpVRq6KWhq6KikJ2H1idowOqeUCxhsDQEAyxADS3gO1AERMFuhjdzQMVflSEWNwHABLVzCNAcsV4AAAAAElFTkSuQmCC',
			'58A7' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAc0lEQVR4nGNYhQEaGAYTpIn7QkMYQximMIaGIIkFNLC2MoQyNIigiIk0Ojo6oIgFBrC2sgJlApDcFzZtZdjSVVErs5Dd1wpW14pic6tIo2towBRksQCQWENAALKYyBSQ3kAHZDHWAMYQdLGBCj8qQizuAwAkVszU9SE+TwAAAABJRU5ErkJggg==',
			'3F1C' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAY0lEQVR4nGNYhQEaGAYTpIn7RANEQx2mMEwNQBILmCLSwBDCECCCrLJVpIExhNGBBVkMpG4KowOy+1ZGTQ1bNW1lFor7UNXBzcMlhmwH2C1TUN0iCuQxhjqguHmgwo+KEIv7ANpJyl/HL6/wAAAAAElFTkSuQmCC',
			'2021' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbUlEQVR4nM2QsRGAMAhFScEGGShu8AtZIlNgwQaaDSzilFKG01Lvwu8ecLyDrkcpzZRf/BgEErKR5T2taSnHyGBsrJCwbXkr3gl+rfXa6xn84HMWb6TibI+Mlc1toou6S4lMhMACwQT/+zAvfjc6r8qbku3bMgAAAABJRU5ErkJggg==',
			'1350' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcklEQVR4nGNYhQEaGAYTpIn7GB1YQ1hDHVqRxVgdRFpZGximOiCJiTowNLo2MAQEoOhlaGWdyuggguS+lVmrwpZmZmZNQ3IfSB1DQyBMHUys0QGLmGtDAJodIq2Mjg6obglhDWEIZUBx80CFHxUhFvcBACkhyQzMoJmgAAAAAElFTkSuQmCC',
			'1D8B' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXElEQVR4nGNYhQEaGAYTpIn7GB1EQxhCGUMdkMRYHURaGR0dHQKQxEQdRBpdGwIdRFD0ijQ6ItSBnbQya9rKrNCVoVlI7kNTBxfDZh4WMUy3hGC6eaDCj4oQi/sAOzPJGuyI++kAAAAASUVORK5CYII=',
			'3159' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcUlEQVR4nGNYhQEaGAYTpIn7RAMYAlhDHaY6IIkFTGEMYG1gCAhAVtnKChRjdBBBFpsC1DsVLgZ20sqoVVFLM7OiwpDdB1QHNHUqit5WsFgDuhhrQwCKHQFAvYyODihuEQW6mCGUAcXNAxV+VIRY3AcAiGXJUh8PFf0AAAAASUVORK5CYII=',
			'082A' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcklEQVR4nGNYhQEaGAYTpIn7GB0YQxhCGVqRxVgDWFsZHR2mOiCJiUwRaXRtCAgIQBILaGUF6gt0EEFyX9TSlWGrVmZmTUNyH1hdKyNMHVRMpNFhCmNoCJodDgGo6sBucUAVA7mZNTQQRWygwo+KEIv7AD7kym5ddHi8AAAAAElFTkSuQmCC',
			'1995' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcUlEQVR4nGNYhQEaGAYTpIn7GB0YQxhCGUMDkMRYHVhbGR0dHZDViTqINLo2BDqg6gWLuToguW9l1tKlmZmRUVFI7gPaEegQEtAggqKXodGhAV2MpdERaAeqGMgtDgHI7hMNAbmZYarDIAg/KkIs7gMAsk7IzQJhRP0AAAAASUVORK5CYII=',
			'9C19' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcUlEQVR4nGNYhQEaGAYTpIn7WAMYQxmmMEx1QBITmcLa6BDCEBCAJBbQKtLgGMLoIIImxjAFLgZ20rSp04DEqqgwJPexuoLUMUxF1ssA1gu0C0lMACjmMIUBxQ6wW6agugXkZsZQBxQ3D1T4URFicR8AqQjL0P3S1/0AAAAASUVORK5CYII=',
			'1DA3' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaklEQVR4nGNYhQEaGAYTpIn7GB1EQximMIQ6IImxOoi0MoQyOgQgiYk6iDQ6Ojo0iKDoFWl0bQhoCEBy38qsaStTV0UtzUJyH5o6hFhoAFbz0MRaWRsCUd0SIhrC2hCA4uaBCj8qQizuAwA0wMux4tU9hQAAAABJRU5ErkJggg==',
			'E2A2' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcUlEQVR4nM2QsQ3AIAwEn4INyD5Q0DsSbpgGCjYgI9AwZUgVrKRMJPzd+SWfjP6YhJXyix8HFVBx2IlR0gUMIsFMds5ZIxiyvzaTH8feWo8jt9/oVZ0oyxsgzVQgmLKjVyXTI0TSeWOfdg4L/O/DvPidiu/OMXYueF0AAAAASUVORK5CYII=',
			'EC46' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZklEQVR4nGNYhQEaGAYTpIn7QkMYQxkaHaY6IIkFNLA2OrQ6BASgiIk0OEx1dBBAE2MIdHRAdl9o1LRVKzMzU7OQ3AdSx9roiGEea2iggwi6HY2OaGJAtzSiugWbmwcq/KgIsbgPABKjzojeAQMLAAAAAElFTkSuQmCC',
			'A5E0' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAa0lEQVR4nGNYhQEaGAYTpIn7GB1EQ1lDHVqRxVgDRBpYGximOiCJiUwBiwUEIIkFtIqEsAJNEEFyX9TSqUuXhq7MmobkvoBWhkZXhDowDA3FFAOaBxRDt4O1Fd0tAa2MIehuHqjwoyLE4j4AyXzMMtbtyj8AAAAASUVORK5CYII=',
			'0BD3' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXElEQVR4nGNYhQEaGAYTpIn7GB1EQ1hDGUIdkMRYA0RaWRsdHQKQxESmiDS6NgQ0iCCJBbQC1QHFApDcF7V0athSIJmF5D40dTAxDPOw2YHNLdjcPFDhR0WIxX0A+ozNrjj6i7AAAAAASUVORK5CYII=',
			'0895' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcUlEQVR4nGNYhQEaGAYTpIn7GB0YQxhCGUMDkMRYA1hbGR0dHZDViUwRaXRtCEQRC2hlbWVtCHR1QHJf1NKVYSszI6OikNwHUscQEtAggqJXpNGhAVUMZIcj0A4RDLc4BCC7D+JmhqkOgyD8qAixuA8AW3jK8XRJRuMAAAAASUVORK5CYII=',
			'C565' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdUlEQVR4nM2QvRGAMAhGoWAD3IcU6eNdaDINFhkhI1jolMaOqKXeyde94+cdsN/K4E/5xI/ypKCoyTGubBiC+L60sJFdmHEmwyjOr+xtXdtWivPru5cYxHiY7ezcMN7obBYeXKhikOT9KGMGhSY/+N+LefA7ALf9zA7StxyVAAAAAElFTkSuQmCC',
			'1863' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZUlEQVR4nGNYhQEaGAYTpIn7GB0YQxhCGUIdkMRYHVhbGR0dHQKQxEQdRBpdGxwaRFD0srayAukAJPetzFoZtnTqqqVZSO4Dq3N0aAhA0QsyLwDNPGxiWNwSgunmgQo/KkIs7gMASArJ+pXrYAIAAAAASUVORK5CYII=',
			'9A66' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAd0lEQVR4nGNYhQEaGAYTpIn7WAMYAhhCGaY6IImJTGEMYXR0CAhAEgtoZW1lbXB0EEARE2l0bWB0QHbftKnTVqZOXZmaheQ+VlegOkdHFPMYWkVDXRsCHUSQxATA5qGKiUwRaXREcwtrgEijA5qbByr8qAixuA8AwzvMIBFldqgAAAAASUVORK5CYII=',
			'0ECA' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZElEQVR4nGNYhQEaGAYTpIn7GB1EQxlCHVqRxVgDRIDiAVMdkMREpog0sDYIBAQgiQW0gsQYHUSQ3Be1dGrY0lUrs6YhuQ9NHbJYaAiGHYIo6iBuCUQRg7jZEUVsoMKPihCL+wAayMpaHrQ8ywAAAABJRU5ErkJggg==',
			'FDC8' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAWElEQVR4nGNYhQEaGAYTpIn7QkNFQxhCHaY6IIkFNIi0MjoEBASgijW6Ngg6iGCIMcDUgZ0UGjVtZeqqVVOzkNyHpg5JjBGLeRh2YHELppsHKvyoCLG4DwD90c52Gul03gAAAABJRU5ErkJggg==',
			'AAA4' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZ0lEQVR4nGNYhQEaGAYTpIn7GB0YAhimMDQEIImxBjCGMIQyNCKLiUxhbWV0dGhFFgtoFWl0bQiYEoDkvqil01amroqKikJyH0RdoAOy3tBQ0VDX0MDQEEzzGrDYQVBsoMKPihCL+wDmac/8CaAiHwAAAABJRU5ErkJggg==',
			'748C' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAa0lEQVR4nGNYhQEaGAYTpIn7QkMZWhlCGaYGIIu2MkxldHQIEEEVC2VtCHRgQRabwujK6OjogOK+qKVLV4WuzEJ2H6ODSCuSOjBkbRANdQWahywGtK8V3Q6gu1rR3RLQgMXNAxR+VIRY3AcAdwTKLul+4NwAAAAASUVORK5CYII=',
			'F710' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaUlEQVR4nGNYhQEaGAYTpIn7QkNFQx2mMLQiiwU0MDQ6hDBMdUATcwxhCAhAFWtlmMLoIILkvtCoVdNWTVuZNQ3JfUB1AUjqoGKMDphirA0MU9DtEAGJoblFpIEx1AHFzQMVflSEWNwHABTwzOZSR930AAAAAElFTkSuQmCC',
			'6740' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdElEQVR4nGNYhQEaGAYTpIn7WANEQx0aHVqRxUSmMABFHKY6IIkFtADFpjoEBCCLNTC0MgQ6OogguS8yatW0lZmZWdOQ3BcyhSGAtRGuDqK3ldGBNTQQTYy1AWgLih0iU0QawDajuBkshuLmgQo/KkIs7gMA2ODNanKlE4MAAAAASUVORK5CYII=',
			'8319' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbElEQVR4nGNYhQEaGAYTpIn7WANYQximMEx1QBITmSLSyhDCEBCAJBbQytDoGMLoIIKijqGVYQpcDOykpVGrwlZNWxUVhuQ+iDqGqSJo5jlMAcphiqHZIQLSi+IWkJsZQx1Q3DxQ4UdFiMV9AGWoy7rSZRAHAAAAAElFTkSuQmCC',
			'D0A7' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaUlEQVR4nGNYhQEaGAYTpIn7QgMYAhimMIaGIIkFTGEMYQhlaBBBFmtlbWV0dEATE2l0bQgAQoT7opZOW5m6KmplFpL7oOpaGdD1hgZMYUCzg7UhIIABzS2sDYEO6G5GFxuo8KMixOI+AFuPzeg1RBS9AAAAAElFTkSuQmCC',
			'61F5' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZUlEQVR4nGNYhQEaGAYTpIn7WAMYAlhDA0MDkMREpjAGsDYwOiCrC2hhxRRrYACJuToguS8yalXU0tCVUVFI7guZAlIHNBdZbysuMUYHZDERiN4AZPcBXRIKFJvqMAjCj4oQi/sA1tzI278P4O4AAAAASUVORK5CYII=',
			'B59F' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbElEQVR4nGNYhQEaGAYTpIn7QgNEQxlCGUNDkMQCpog0MDo6OiCrC2gVaWBtCEQVmyISgiQGdlJo1NSlKzMjQ7OQ3BcwhaHRIQRNbytQDN28VpFGRww7WFvR3RIawBgCdDOK2ECFHxUhFvcBABMEyzYk8YRBAAAAAElFTkSuQmCC',
			'284E' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbElEQVR4nGNYhQEaGAYTpIn7WAMYQxgaHUMDkMREprC2MrQ6OiCrC2gVaXSYiirG0ApUFwgXg7hp2sqwlZmZoVnI7gtgbWVtRNXL6CDS6BoaiCLG2gC0A02dSAPQDjSx0FBMNw9U+FERYnEfAPrzynIIOPWIAAAAAElFTkSuQmCC',
			'2B52' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAeUlEQVR4nM2QsRGAMAhFkyIb4D6xsKcIFm6gU5CCDRI3sMmUYoenpd4FKv59Pu9w7VHseupf+AIOKVCs0WhQQAI7RKOhQJ7YR7Dbor6qfsu31/lYt7ZYPgTRhGxvaJLOKDcWvm5gsRowiB8jWo1oSI48pQ7+92G/8J0SCMwOddu2UgAAAABJRU5ErkJggg==',
			'2455' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdElEQVR4nM2QvRGAMAhGScEGuA8p0mNBkw2yBU02MG6QximNHaeWehe+7h0/74DjUQYz5Rc/FKioUcUx2qChBfZ9UkHvDGpI2EJi77f33kvJ2fsJ1bHByM0GXpRvDMdGtJU9o+tKZPF+qjBsoPEE//swL34no0vKXEn+Ga8AAAAASUVORK5CYII=',
			'2E18' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaklEQVR4nGNYhQEaGAYTpIn7WANEQxmmMEx1QBITmSLSwBDCEBCAJBbQKtLAGMLoIIKsGygG1AtTB3HTtKlhq6atmpqF7L4AFHVgCDZpCqp5rA2YYiINmHpDQ0VDGUMdUNw8UOFHRYjFfQBZUsrKKsY2xAAAAABJRU5ErkJggg==',
			'4B5C' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdUlEQVR4nGNYhQEaGAYTpI37poiGsIY6TA1AFgsRaWVtYAgQQRJjDBFpdG1gdGBBEmOdAlQ3ldEB2X3Tpk0NW5qZmYXsvgCgOoaGQAdke0NDRRod0MQYpoDsCESxAyjWyujogOIWkJsZQhlQ3TxQ4Uc9iMV9AMCgyytTAAKvAAAAAElFTkSuQmCC',
			'9F14' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbUlEQVR4nGNYhQEaGAYTpIn7WANEQx2mMDQEIImJTBFpYAhhaEQWC2gVaWAMYWhFF2OYwjAlAMl906ZODVs1bVVUFJL7WF1B6hgdkPUygPUyhoYgiQlAzMN0C5oYawDQLaEOKGIDFX5UhFjcBwBDgc0NcNqz7QAAAABJRU5ErkJggg==',
			'27CF' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAb0lEQVR4nGNYhQEaGAYTpIn7WANEQx1CHUNDkMREpjA0OjoEOiCrC2hlaHRtEEQRY2hlaGVtYISJQdw0bdW0patWhmYhuy+AIQBJHRgyOjA6oIuxgiGqHSJAyIjmltBQkQaGUEdUtwxQ+FERYnEfAFb0yNPi6U6DAAAAAElFTkSuQmCC',
			'4918' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcElEQVR4nGNYhQEaGAYTpI37pjCGMExhmOqALBbC2soQwhAQgCTGGCLS6BjC6CCCJMY6RaTRYQpcHdhJ06YtXZo1bdXULCT3BUxhDERSB4ahoQxAvajmMUxhwSIGdAuaXpCbGUMdUN08UOFHPYjFfQCizcvqXuF1cQAAAABJRU5ErkJggg==',
			'5091' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbUlEQVR4nGNYhQEaGAYTpIn7QkMYAhhCGVqRxQIaGEMYHR2mooqxtrI2BIQiiwUGiDS6NgTA9IKdFDZt2srMzKilKO5rFWl0CAlAsQMs1oAqFtDK2sqIJiYyBewWFDHWALCbQwMGQfhREWJxHwDeYMvepyb7qgAAAABJRU5ErkJggg==',
			'EC85' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAYUlEQVR4nGNYhQEaGAYTpIn7QkMYQxlCGUMDkMQCGlgbHR0dHRhQxEQaXBsCMcQYHR1dHZDcFxo1DUisjIpCch9EnQOQRNXLCiYx7RDBcItDALL7IG5mmOowCMKPihCL+wD8ac0FJVPpvQAAAABJRU5ErkJggg==',
			'00C3' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXUlEQVR4nGNYhQEaGAYTpIn7GB0YAhhCHUIdkMRYAxhDGB0CHQKQxESmsLayNgg0iCCJBbSKNLqCaCT3RS2dtjJ11aqlWUjuQ1OHIiZCwA5sbsHm5oEKPypCLO4DALi3y9YU/i6YAAAAAElFTkSuQmCC',
			'D2DD' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZElEQVR4nGNYhQEaGAYTpIn7QgMYQ1hDGUMdkMQCprC2sjY6OgQgi7WKNLo2BDqIoIgxIIuBnRS1dNXSpasis6YhuQ+obgorpt4ATDFGBwwxoE50t4QGiIa6orl5oMKPihCL+wASXM2l1op6FQAAAABJRU5ErkJggg==',
			'4A43' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcElEQVR4nGNYhQEaGAYTpI37pjAEMDQ6hDogi4UwhjC0OjoEIIkxhrC2Mkx1aBBBEmOdItLoEOjQEIDkvmnTpq3MzMxamoXkvgCgOtdGuDowDA0VDXUNDUAxjwFkXqMDFjFUt0DVobp5oMKPehCL+wBx585qCs0EVAAAAABJRU5ErkJggg==',
			'7481' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaElEQVR4nGNYhQEaGAYTpIn7QkMZWhlAGFm0lWEqo6PDVDSxUNaGgFAUsSmMrkB1ML0QN0UtXboqdNVSZPcxOoi0IqkDQ9YG0VDXhgAUMRGgLaxoYgFAMXS9AWDXMIQGDILwoyLE4j4AsGHLONu37o0AAAAASUVORK5CYII=',
			'2C95' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdUlEQVR4nGNYhQEaGAYTpIn7WAMYQxlCGUMDkMREprA2Ojo6OiCrC2gVaXBtCEQRYwCKsTYEujogu2/atFUrMyOjopDdFyDSwBAS0CCCpJfRAcRDFWMF8hyBdiCLAW0AusUhANl9oaEgNzNMdRgE4UdFiMV9AAnPy25DmREjAAAAAElFTkSuQmCC',
			'EAAF' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXklEQVR4nGNYhQEaGAYTpIn7QkMYAhimMIaGIIkFNDCGMIQyOjCgiLG2Mjo6oomJNLo2BMLEwE4KjZq2MnVVZGgWkvvQ1EHFRENdQ9HFsKnDFAsNwRQbqPCjIsTiPgDec8xU5obaswAAAABJRU5ErkJggg==',
			'726D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAb0lEQVR4nGNYhQEaGAYTpIn7QkMZQxhCGUMdkEVbWVsZHR0dAlDERBpdGxwdRJDFpjAAxRhhYhA3Ra1aunTqyqxpSO5jdGCYwuqIqpe1gSGAtSEQRUwEqBJdLACoEt0tAQ2ioQ7obh6g8KMixOI+AC8xyqtefZFdAAAAAElFTkSuQmCC',
			'BB37' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXklEQVR4nGNYhQEaGAYTpIn7QgNEQxhDGUNDkMQCpoi0sjY6NIggi7WKAEUCUMWA6hjAogj3hUZNDVs1ddXKLCT3QdW1MmCaNwWLWAADhlscHbC4GUVsoMKPihCL+wCIOs6mzlCspwAAAABJRU5ErkJggg==',
			'BB5F' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZElEQVR4nGNYhQEaGAYTpIn7QgNEQ1hDHUNDkMQCpoi0sjYwOiCrC2gVaXRFFwOpmwoXAzspNGpq2NLMzNAsJPeB1DE0BGKY54BFzBVdDKiX0dERRQzkZoZQVLcMVPhREWJxHwBT28tlGu84PQAAAABJRU5ErkJggg==',
			'B25C' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdElEQVR4nGNYhQEaGAYTpIn7QgMYQ1hDHaYGIIkFTGFtZW1gCBBBFmsVaXRtYHRgQVHH0Og6ldEB2X2hUauWLs3MzEJ2H1DdFIaGQAcGFPMYAjDFGB1YgWKodrA2MDo6oLglNEA01CGUAcXNAxV+VIRY3AcAfVzMXzyB8B4AAAAASUVORK5CYII=',
			'429F' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbUlEQVR4nGNYhQEaGAYTpI37pjCGMIQyhoYgi4WwtjI6Ojogq2MMEWl0bQhEEWOdwoAsBnbStGmrlq7MjAzNQnJfwBSGKQwhqHpDQxkCGNDMA7rFgRFDjLUB3S0MU0RDHUIZUcUGKvyoB7G4DwB6TskosdmMYgAAAABJRU5ErkJggg==',
			'45AF' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbElEQVR4nGNYhQEaGAYTpI37poiGMkxhDA1BFgsRaWAIZXRAVscIFGN0dEQRY50iEsLaEAgTAztp2rSpS5euigzNQnJfwBSGRleEOjAMDQWKhaKKMUwRwVDHMIW1lRVDjDEEQ2ygwo96EIv7AJLMyljy3PUrAAAAAElFTkSuQmCC',
			'1E31' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXElEQVR4nGNYhQEaGAYTpIn7GB1EQxlDGVqRxVgdRBpYGx2mIouJAsUYGgJCUfUCxRodYHrBTlqZNTVs1dRVS5Hdh6YOIdYQQJQYK5pe0RCwm0MDBkH4URFicR8AAfXJwSRaC4oAAAAASUVORK5CYII=',
			'4656' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAeElEQVR4nGNYhQEaGAYTpI37pjCGsIY6THVAFgthbWVtYAgIQBJjDBFpZG1gdBBAEmOdItLAOpXRAdl906ZNC1uamZmaheS+gCmirQwNgSjmhYaKNDo0BDqIoLhFpNEVQ4y1ldHRAUUvyM0MoQyobh6o8KMexOI+AMdqy0VQo/SoAAAAAElFTkSuQmCC',
			'E3B1' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAVUlEQVR4nGNYhQEaGAYTpIn7QkNYQ1hDGVqRxQIaRFpZGx2moooxNLo2BISiiYHUwfSCnRQatSpsaeiqpcjuQ1OHbB4RYiIYeqFuDg0YBOFHRYjFfQB0Fc4YyiJ89wAAAABJRU5ErkJggg==',
			'4C92' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcUlEQVR4nGNYhQEaGAYTpI37pjCGMoQyTHVAFgthbXR0dAgIQBJjDBFpcG0IdBBBEmOdItLA2hDQIILkvmnTpq1amRm1KgrJfQFAdQwhAY3IdoSGgnQFtKK6RaTBEagaVQziFkw3M4aGDIbwox7E4j4Aw4HMsvmqv5oAAAAASUVORK5CYII=',
			'3314' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaElEQVR4nGNYhQEaGAYTpIn7RANYQximMDQEIIkFTBFpZQhhaEQWY2hlaHQMYWhFEZsCFJ3CMCUAyX0ro1aFrZq2KioK2X1gdYwO6OY5TGEMDcEQw+IWNDGQmxlDHVDEBir8qAixuA8ANyLNIkDGci0AAAAASUVORK5CYII=',
			'B71D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbElEQVR4nGNYhQEaGAYTpIn7QgNEQx2mMIY6IIkFTGFodAhhdAhAFmtlaHQEiomgqmtlmAIXAzspNGrVtFXTVmZNQ3IfUF0AkjqoeYwOmGKsDRhiU0TAYshuCQ0QaWAMdURx80CFHxUhFvcBACZbzBmp1hrqAAAAAElFTkSuQmCC',
			'1981' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZklEQVR4nGNYhQEaGAYTpIn7GB0YQxhCGVqRxVgdWFsZHR2mIouJOog0ujYEhKLqFWl0dHSA6QU7aWXW0qVZoauWIrsPaEcgkjqoGAPIPDQxFixiYLegiImGgN0cGjAIwo+KEIv7AGi4yUeXoqhUAAAAAElFTkSuQmCC',
			'F339' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAYElEQVR4nGNYhQEaGAYTpIn7QkNZQxhDGaY6IIkFNIi0sjY6BASgiDE0OjQEOoigirUyNDrCxMBOCo1aFbZq6qqoMCT3QdQ5TBXBMA9oE6YYmh3Y3ILp5oEKPypCLO4DAJkZzj3I+KzzAAAAAElFTkSuQmCC',
			'19A3' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdElEQVR4nGNYhQEaGAYTpIn7GB0YQximMIQ6IImxOrC2MoQyOgQgiYk6iDQ6Ojo0iKDoFWl0bQhoCEBy38qspUtTV0UtzUJyH9COQCR1UDGGRtfQADTzWMDmoYqxtrI2BKK6JYQxhLUhAMXNAxV+VIRY3AcAcaXLD+ZVixkAAAAASUVORK5CYII=',
			'2496' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbUlEQVR4nGNYhQEaGAYTpIn7WAMYWhlCGaY6IImJTGGYyujoEBCAJBYAVMXaEOgggKy7ldEVJIbivmlLl67MjEzNQnZfgEgrQ0gginmMDqKhDkC9IshuAZmIJiYCEkNzS2goppsHKvyoCLG4DwApzsqlidR1dwAAAABJRU5ErkJggg==',
			'3EF5' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAX0lEQVR4nGNYhQEaGAYTpIn7RANEQ1lDA0MDkMQCpog0sDYwOqCobMUiBlHn6oDkvpVRU8OWhq6MikJ2H1gdQ4MIhnnYxBgdkMUgbmEIQHYf2M0NDFMdBkH4URFicR8AgprKK/+rujgAAAAASUVORK5CYII=',
			'7921' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcElEQVR4nGNYhQEaGAYTpIn7QkMZQxhCGVpRRFtZWxkdHaaiiok0ujYEhKKITRFpdGgIgOmFuClq6dKslVlLkd3H6MAY6NCKagdrA0OjwxRUMZEGlkaHAFSxgAagWxzQxRhDWEMDQgMGQfhREWJxHwBr/sukxMUunQAAAABJRU5ErkJggg==',
			'1F16' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAYUlEQVR4nGNYhQEaGAYTpIn7GB1EQx2mMEx1QBJjdRBpYAhhCAhAEhMFijGGMDoIoOgFqpvC6IDsvpVZU8NWTVuZmoXkPqg6FPNgekWIEkNzSwjQLaEOKG4eqPCjIsTiPgAuP8hSu540zgAAAABJRU5ErkJggg==',
			'A03D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZElEQVR4nGNYhQEaGAYTpIn7GB0YAhhDGUMdkMRYAxhDWBsdHQKQxESmsLYyNAQ6iCCJBbSKNDoA1YkguS9q6bSVWVNXZk1Dch+aOjAMDQWKYZiHzQ5MtwS0Yrp5oMKPihCL+wDivsxAQIGdxwAAAABJRU5ErkJggg==',
			'2B81' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAY0lEQVR4nGNYhQEaGAYTpIn7WANEQxhCGVqRxUSmiLQyOjpMRRYLaBVpdG0ICEXR3QpWB9MLcdO0qWGrQlctRXFfAIo6MGR0AJuHIsbagCkm0oCpNzQU7ObQgEEQflSEWNwHAGrny6xyKZIBAAAAAElFTkSuQmCC',
			'BD01' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAWklEQVR4nGNYhQEaGAYTpIn7QgNEQximMLQiiwVMEWllCGWYiiLWKtLo6OgQiqau0RUog+y+0KhpK1NXRS1Fdh+aOrh52MSAdmBzC4oY1M2hAYMg/KgIsbgPAPdgznn7iy+5AAAAAElFTkSuQmCC',
			'CE11' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXUlEQVR4nGNYhQEaGAYTpIn7WENEQxmmMLQii4m0ijQwhDBMRRYLaBRpYAxhCEURawCqQ+gFOylq1dSwVdNWLUV2H5o63GKNmGJgt6CJgdzMGOoQGjAIwo+KEIv7AF9zy617kUKsAAAAAElFTkSuQmCC',
			'98F3' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAX0lEQVR4nGNYhQEaGAYTpIn7WAMYQ1hDA0IdkMREprC2sjYwOgQgiQW0ijS6guRQxEDqgDSS+6ZNXRm2NHTV0iwk97G6oqiDQCzmCWARw+YWsJsbGFDcPFDhR0WIxX0ACmXL+9he8OoAAAAASUVORK5CYII=',
			'44A8' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbUlEQVR4nGNYhQEaGAYTpI37pjC0AvFUB2SxEIapDKEMAQFIYowhDKGMjo4OIkhirFMYXVkbAmDqwE6aNm3p0qWroqZmIbkvYIpIK5I6MAwNFQ11DQ1EMQ/kFtYGbGKoeqFiqG4eqPCjHsTiPgAQyMx3L3xqhwAAAABJRU5ErkJggg==',
			'584F' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbUlEQVR4nGNYhQEaGAYTpIn7QkMYQxgaHUNDkMQCGlhbGVodHRhQxEQaHaaiigUGANUFwsXATgqbtjJsZWZmaBay+1pZW1kbUfUytIo0uoYGotoBFHNAUycyBWgHmhhrANjNqOYNUPhREWJxHwDJ8crYqGwYCAAAAABJRU5ErkJggg==',
			'9DB7' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZUlEQVR4nGNYhQEaGAYTpIn7WANEQ1hDGUNDkMREpoi0sjY6NIggiQW0ijS6NgRgigHVBSC5b9rUaStTQ1etzEJyH6srWF0ris0Q86YgiwlAxAIYMNzi6IDFzShiAxV+VIRY3AcA2sbNOVCKVyUAAAAASUVORK5CYII=',
			'7D98' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaElEQVR4nGNYhQEaGAYTpIn7QkNFQxhCGaY6IIu2irQyOjoEBKCKNbo2BDqIIItNAYkFwNRB3BQ1bWVmZtTULCT3MTqINDqEBKCYx9oAFEMzTwQo5ogmFtCA6ZaABixuHqDwoyLE4j4A+tTM7nFqm50AAAAASUVORK5CYII=',
			'705D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpIn7QkMZAlhDHUMdkEVbGUNYGxgdAlDEWFtBYiLIYlNEGl2nwsUgboqatjI1MzNrGpL7gCoaHRoCUfSyNmCKiTSA7EAVC2hgDGF0dERxC5AdwBDKiOrmAQo/KkIs7gMAzVbKZfT7u8kAAAAASUVORK5CYII=',
			'A48F' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaElEQVR4nGNYhQEaGAYTpIn7GB0YWhlCGUNDkMRYAximMjo6OiCrE5nCEMraEIgiFtDK6IqkDuykqKVLl64KXRmaheS+gFaRVnTzQkNFQ10xzGNoxbSDAUMvSAzoZhSxgQo/KkIs7gMAfRnJdu04uT0AAAAASUVORK5CYII=',
			'3E0F' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXElEQVR4nGNYhQEaGAYTpIn7RANEQxmmMIaGIIkFTBFpYAhldEBR2SrSwOjoiCoGVMfaEAgTAztpZdTUsKWrIkOzkN2Hqg5uHjYxdDuwuQXqZlS9AxR+VIRY3AcAHq7I0A83yHcAAAAASUVORK5CYII=',
			'6392' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcElEQVR4nGNYhQEaGAYTpIn7WANYQxhCGaY6IImJTBFpZXR0CAhAEgtoYWh0bQh0EEEWa2BoZQWSIkjui4xaFbYyM2pVFJL7QqYwtDKEBDQi2xHQygDkA0k0MceGgCkMWNyC6WbG0JBBEH5UhFjcBwC9v8yezoTDogAAAABJRU5ErkJggg==',
			'2943' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdElEQVR4nGNYhQEaGAYTpIn7WAMYQxgaHUIdkMREprC2MrQ6OgQgiQW0ijQ6THVoEEHWDRILdGgIQHbftKVLMzOzlmYhuy+AMdC1Ea4ODBkdGBpdQwNQzGNtYGl0aES1Q6QB6JZGVLeEhmK6eaDCj4oQi/sA6/TNkjqdrF0AAAAASUVORK5CYII=',
			'9C5D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAa0lEQVR4nGNYhQEaGAYTpIn7WAMYQ1lDHUMdkMREprA2ujYwOgQgiQW0ijSAxETQxFinwsXATpo2ddqqpZmZWdOQ3MfqClIRiKKXoRVTTABsB6oYyC2Ojo4obgG5mSGUEcXNAxV+VIRY3AcANRHLUnehKI0AAAAASUVORK5CYII=',
			'8C28' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpIn7WAMYQxlCGaY6IImJTGFtdHR0CAhAEgtoFWlwbQh0EEFRB+IFwNSBnbQ0atqqVSuzpmYhuQ+srpUBwzyGKYwo5oHEHAIY0ewAusUBVS/IzayhAShuHqjwoyLE4j4A107MpKxN7mgAAAAASUVORK5CYII=',
			'3EEE' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAATElEQVR4nGNYhQEaGAYTpIn7RANEQ1lDHUMDkMQCpog0sDYwOqCobMUihqoO7KSVUVPDloauDM1Cdh+x5mERw+YWbG4eqPCjIsTiPgDR88iunQbBZAAAAABJRU5ErkJggg==',
			'34F7' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaElEQVR4nGNYhQEaGAYTpIn7RAMYWllDA0NDkMQCpjBMZQXSIsgqWxlCMcSmMLqCxAKQ3LcyaunSpaGrVmYhu2+KSCsryAQU80RDXYEyqGIMIHUByGJAtwDFGB0w3IwmNlDhR0WIxX0AYGXKfphVBwoAAAAASUVORK5CYII=',
			'A4CD' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpIn7GB0YWhlCHUMdkMRYAximMjoEOgQgiYlMYQhlbRB0EEESC2hldGUFmiCC5L6opUCwamXWNCT3BbSKtCKpA8PQUNFQVzSxgFaGVkw7GFrR3QISQ3fzQIUfFSEW9wEAdezLIJ0j75EAAAAASUVORK5CYII=',
			'1F32' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaklEQVR4nGNYhQEaGAYTpIn7GB1EQx1DGaY6IImxOog0sDY6BAQgiYkCxRgaAsEkQi+Q1+jQIILkvpVZU8NWTV21KgrJfVB1jQ7oehsCWhkwxaagi4HcgiwmGiLSwBjKGBoyCMKPihCL+wD67spl+/UWIQAAAABJRU5ErkJggg==',
			'F36E' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAVklEQVR4nGNYhQEaGAYTpIn7QkNZQxhCGUMDkMQCGkRaGR0dHRhQxBgaXRswxFpZGxhhYmAnhUatCls6dWVoFpL7wOqwmhdIhBg2t2C6eaDCj4oQi/sA1orLMjJAQlQAAAAASUVORK5CYII=',
			'87E0' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaElEQVR4nGNYhQEaGAYTpIn7WANEQ11DHVqRxUSmMDS6NjBMdUASC2gFiwUEoKprZW1gdBBBct/SqFXTloauzJqG5D6gugAkdVDzGB0wxVgbWDHsEAGJobiFNQAohubmgQo/KkIs7gMAtNLLr/PbP0wAAAAASUVORK5CYII=',
			'7964' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpIn7QkMZQxhCGRoCkEVbWVsZHR0aUcVEGl0bHFpRxKaAxBimBCC7L2rp0tSpq6KikNzH6MAY6Oro6ICsl7WBAag3MDQESUykgQUoFoDiloAGsFvQxLC4eYDCj4oQi/sAIHnN4KrTMUQAAAAASUVORK5CYII=',
			'07DD' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbUlEQVR4nGNYhQEaGAYTpIn7GB1EQ11DGUMdkMRYAxgaXRsdHQKQxESmAMUaAh1EkMQCWhlaWRFiYCdFLV01bemqyKxpSO4DqgtgxdDL6IAuJjKFtQFdjDVApIEVzS2MQBWsaG4eqPCjIsTiPgByyMt8bK/U2wAAAABJRU5ErkJggg==',
			'18D8' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXklEQVR4nGNYhQEaGAYTpIn7GB0YQ1hDGaY6IImxOrC2sjY6BAQgiYk6iDS6NgQ6iKDoBaprCICpAztpZdbKsKWroqZmIbkPTR1UDJt5OOxAd0sIppsHKvyoCLG4DwDngspxdDwXBwAAAABJRU5ErkJggg==',
			'C40B' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAa0lEQVR4nGNYhQEaGAYTpIn7WEMYWhmmMIY6IImJtDJMZQhldAhAEgtoBIo4OjqIIIs1MLqyNgTC1IGdFLVq6dKlqyJDs5DcFwA0EUkdVEw01BUoJoJqRyu6HUC3tKK7BZubByr8qAixuA8A9OTLQbYqZz0AAAAASUVORK5CYII=',
			'EE5E' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAWklEQVR4nGNYhQEaGAYTpIn7QkNEQ1lDHUMDkMQCGkQaWBsYHRiIEZsKFwM7KTRqatjSzMzQLCT3gdQxNARi6MUmxopFjNHREUUM5GaGUEYUNw9U+FERYnEfADTZypoIzKVkAAAAAElFTkSuQmCC',
			'116F' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXElEQVR4nGNYhQEaGAYTpIn7GB0YAhhCGUNDkMRYHRgDGB0dHZDViTqwBrA2oIqB9LKCSCT3rcxaFbV06srQLCT3gdU5YtMbSJQYhltCWEOBbkYRG6jwoyLE4j4AWrzER4Lp9KEAAAAASUVORK5CYII='        
        );
        $this->text = array_rand( $images );
        return $images[ $this->text ] ;    
    }
    
    function out_processing_gif(){
        $image = dirname(__FILE__) . '/processing.gif';
        $base64_image = "R0lGODlhFAAUALMIAPh2AP+TMsZiALlcAKNOAOp4ANVqAP+PFv///wAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFCgAIACwAAAAAFAAUAAAEUxDJSau9iBDMtebTMEjehgTBJYqkiaLWOlZvGs8WDO6UIPCHw8TnAwWDEuKPcxQml0Ynj2cwYACAS7VqwWItWyuiUJB4s2AxmWxGg9bl6YQtl0cAACH5BAUKAAgALAEAAQASABIAAAROEMkpx6A4W5upENUmEQT2feFIltMJYivbvhnZ3Z1h4FMQIDodz+cL7nDEn5CH8DGZhcLtcMBEoxkqlXKVIgAAibbK9YLBYvLtHH5K0J0IACH5BAUKAAgALAEAAQASABIAAAROEMkphaA4W5upMdUmDQP2feFIltMJYivbvhnZ3V1R4BNBIDodz+cL7nDEn5CH8DGZAMAtEMBEoxkqlXKVIg4HibbK9YLBYvLtHH5K0J0IACH5BAUKAAgALAEAAQASABIAAAROEMkpjaE4W5tpKdUmCQL2feFIltMJYivbvhnZ3R0A4NMwIDodz+cL7nDEn5CH8DGZh8ONQMBEoxkqlXKVIgIBibbK9YLBYvLtHH5K0J0IACH5BAUKAAgALAEAAQASABIAAAROEMkpS6E4W5spANUmGQb2feFIltMJYivbvhnZ3d1x4JMgIDodz+cL7nDEn5CH8DGZgcBtMMBEoxkqlXKVIggEibbK9YLBYvLtHH5K0J0IACH5BAUKAAgALAEAAQASABIAAAROEMkpAaA4W5vpOdUmFQX2feFIltMJYivbvhnZ3V0Q4JNhIDodz+cL7nDEn5CH8DGZBMJNIMBEoxkqlXKVIgYDibbK9YLBYvLtHH5K0J0IACH5BAUKAAgALAEAAQASABIAAAROEMkpz6E4W5tpCNUmAQD2feFIltMJYivbvhnZ3R1B4FNRIDodz+cL7nDEn5CH8DGZg8HNYMBEoxkqlXKVIgQCibbK9YLBYvLtHH5K0J0IACH5BAkKAAgALAEAAQASABIAAAROEMkpQ6A4W5spIdUmHQf2feFIltMJYivbvhnZ3d0w4BMAIDodz+cL7nDEn5CH8DGZAsGtUMBEoxkqlXKVIgwGibbK9YLBYvLtHH5K0J0IADs=";
        $binary = is_file($image) ? join("",file($image)) : base64_decode($base64_image); 
        header("Cache-Control: post-check=0, pre-check=0, max-age=0, no-store, no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: image/gif");
        echo $binary;
    }

}
# end of class phpfmgImage
# ------------------------------------------------------
# end of module : captcha


# module user
# ------------------------------------------------------
function phpfmg_user_isLogin(){
    return ( isset($_SESSION['authenticated']) && true === $_SESSION['authenticated'] );
}


function phpfmg_user_logout(){
    session_destroy();
    header("Location: admin.php");
}

function phpfmg_user_login()
{
    if( phpfmg_user_isLogin() ){
        return true ;
    };
    
    $sErr = "" ;
    if( 'Y' == $_POST['formmail_submit'] ){
        if(
            defined( 'PHPFMG_USER' ) && PHPFMG_USER == $_POST['Username'] &&
            defined( 'PHPFMG_PW' )   && PHPFMG_PW   == $_POST['Password']
        ){
             $_SESSION['authenticated'] = true ;
             return true ;
             
        }else{
            $sErr = 'Login failed. Please try again.';
        }
    };
    
    // show login form 
    phpfmg_admin_header();
?>
<form name="frmFormMail" action="" method='post' enctype='multipart/form-data'>
<input type='hidden' name='formmail_submit' value='Y'>
<br><br><br>

<center>
<div style="width:380px;height:260px;">
<fieldset style="padding:18px;" >
<table cellspacing='3' cellpadding='3' border='0' >
	<tr>
		<td class="form_field" valign='top' align='right'>Email :</td>
		<td class="form_text">
            <input type="text" name="Username"  value="<?php echo $_POST['Username']; ?>" class='text_box' >
		</td>
	</tr>

	<tr>
		<td class="form_field" valign='top' align='right'>Password :</td>
		<td class="form_text">
            <input type="password" name="Password"  value="" class='text_box'>
		</td>
	</tr>

	<tr><td colspan=3 align='center'>
        <input type='submit' value='Login'><br><br>
        <?php if( $sErr ) echo "<span style='color:red;font-weight:bold;'>{$sErr}</span><br><br>\n"; ?>
        <a href="../../assests/sfd2009/register/admin.php?mod=mail&func=password">I forgot my password</a>    
    </td></tr>
</table>
</fieldset>
</div>
<script type="text/javascript">
    document.frmFormMail.Username.focus();
</script>
</form>
<?php
    phpfmg_admin_footer();
}


function phpfmg_mail_password(){
    phpfmg_admin_header();
    if( defined( 'PHPFMG_USER' ) && defined( 'PHPFMG_PW' ) ){
        mail( PHPFMG_USER, "Your password", "Here is the password for your form admin panel:\n\nUsername: " . PHPFMG_USER . "\nPassword: " . PHPFMG_PW . "\n\n" );
        echo "<center>Your password has been sent.<br><br><a href='admin.php'>Click here to login again</a></center>";
    };   
    phpfmg_admin_footer();
}


function phpfmg_writable_check(){
 
    if( is_writable( dirname(PHPFMG_SAVE_FILE) ) && is_writable( dirname(PHPFMG_EMAILS_LOGFILE) )  ){
        return ;
    };
?>
<style type="text/css">
    .fmg_warning{
        background-color: #F4F6E5;
        border: 1px dashed #ff0000;
        padding: 16px;
        color : black;
        margin: 10px;
        line-height: 180%;
        width:80%;
    }
    
    .fmg_warning_title{
        font-weight: bold;
    }

</style>
<br><br>
<div class="fmg_warning">
    <div class="fmg_warning_title">Your form data or email traffic log is NOT saving.</div>
    The form data (<?php echo PHPFMG_SAVE_FILE ?>) and email traffic log (<?php echo PHPFMG_EMAILS_LOGFILE?>) will be created aumotically when the form is submitted. 
    However, the script doesn't have writable permission to create those files. In order to save your valuable information, please set the directory to writable.
     If you don't know how to do it, please ask for help from your web Administrator or Technical Support of your hosting company.   
</div>
<br><br>
<?php
}


function phpfmg_log_view(){
    $n = isset($_REQUEST['file'])  ? $_REQUEST['file']  : '';
    $files = array(
        1 => PHPFMG_EMAILS_LOGFILE,
        2 => PHPFMG_SAVE_FILE,
    );
    
    phpfmg_admin_header();
   
    $file = $files[$n];
    if( is_file($file) ){
        if( 1== $n ){
            echo "<pre>\n";
            echo join("",file($file) );
            echo "</pre>\n";
        }else{
            $man = new phpfmgDataManager();
            $man->displayRecords();
        };
     

    }else{
        echo "<b>No form data found.</b>";
    };
    phpfmg_admin_footer();
}


function phpfmg_log_download(){
    $n = isset($_REQUEST['file'])  ? $_REQUEST['file']  : '';
    $files = array(
        1 => PHPFMG_EMAILS_LOGFILE,
        2 => PHPFMG_SAVE_FILE,
    );

    $file = $files[$n];
    if( is_file($file) ){
        phpfmg_util_download( $file, PHPFMG_SAVE_FILE == $file ? 'form-data.csv' : 'email-traffics.txt', true, 1 ); // skip the first line
    }else{
        phpfmg_admin_header();
        echo "<b>No email traffic log found.</b>";
        phpfmg_admin_footer();
    };

}




function phpfmg_util_download($file, $filename='', $toCSV = false, $skipN = 0 ){
    if (!is_file($file)) return false ;

    set_time_limit(0);
    while (@ob_end_clean()); // no output buffering !
    
    $len = filesize($file);
    $filename = basename( '' == $filename ? $file : $filename );
    $file_extension = strtolower(substr(strrchr($filename,"."),1));

    switch( $file_extension ) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "jpeg":
        case "jpg": $ctype="image/jpg"; break;
        case "mp3": $ctype="audio/mpeg"; break;
        case "wav": $ctype="audio/x-wav"; break;
        case "mpeg":
        case "mpg":
        case "mpe": $ctype="video/mpeg"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        //The following are for extensions that shouldn't be downloaded (sensitive stuff, like php files)
        case "php":
        case "htm":
        case "html": 
                $ctype="text/plain"; break;
        default: $ctype="application/force-download";
    }
    
    //Begin writing headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public"); 
    header("Content-Description: File Transfer");
    
    //Use the switch-generated Content-Type
    header("Content-Type: $ctype");
    //Force the download
    $header="Content-Disposition: attachment; filename=".$filename.";";
    header($header );
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ".$len);

    $i = 0 ;
    $fp = @fopen($file, 'rb');
    while( !feof($fp) && (0 == connection_status()) ) { 


        flush();
        $i ++ ;
        if( $toCSV ){ 
            $line = fgets($fp);
            if($i > $skipN){ // skip lines
                $line = str_replace( chr(0x09), ',', $line );
                echo phpfmg_data2record( $line, false );
            }; 
            
        }else{
            print( fread($fp, 1024*100) );
        };
        
    }; 
    fclose ($fp);
    
    return true ;
}
?>